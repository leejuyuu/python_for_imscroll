import io
from pathlib import Path

import numpy as np
import pytest

from blink import image_processing as imp
from blink import mapping

TEST_DATA_DIR = Path(__file__).parent / "test_data"

SRC = np.array(
    [
        [-12.3705, -10.5075],
        [-10.7865, 15.4305],
        [8.6985, 10.8675],
        [11.4975, -9.5715],
        [7.8435, 7.4835],
        [-5.3325, 6.5025],
        [6.7905, -6.3765],
        [-6.1695, -0.8235],
    ]
)
DST = np.array(
    [
        [0, 0],
        [0, 5800],
        [4900, 5800],
        [4900, 0],
        [4479, 4580],
        [1176, 3660],
        [3754, 790],
        [1024, 1931],
    ]
)


def test_mapper_from_imscroll():
    path = TEST_DATA_DIR / "mapping/20200206_br_02.dat"
    mapper = mapping.Mapper.from_imscroll(path)
    assert isinstance(mapper, mapping.Mapper)
    assert ("blue", "red") in mapper.map_matrix

    aois = imp.Aois.from_imscroll_aoiinfo2(TEST_DATA_DIR / "mapping/L2_aoi.dat")
    aois.channel = "blue"
    for channel in ("red", "blue"):
        new_aois = mapper.map(aois, to_channel=channel)
        assert isinstance(new_aois, imp.Aois)
        assert new_aois.frame == aois.frame
        assert new_aois.frame_avg == aois.frame_avg
        assert new_aois.width == aois.width
        assert new_aois.channel == channel
        if channel == "red":
            correct_aois = imp.Aois.from_imscroll_aoiinfo2(
                TEST_DATA_DIR / "mapping/L2_map.dat"
            )
            np.testing.assert_allclose(
                new_aois._coords, correct_aois._coords, rtol=1e-6
            )

    with pytest.raises(ValueError) as exception_info:
        new_aois = mapper.map(aois, to_channel="green")
        assert exception_info.value == (
            "Mapping matrix from channel blue" " to channel green is not loaded"
        )

    for to_channel in ("black", 123):
        with pytest.raises(ValueError) as exception_info:
            new_aois = mapper.map(aois, to_channel=to_channel)
            assert (
                exception_info.value
                == "To-channel is not one of the available channels"
            )

    for from_channel in ("black", 123):
        aois = imp.Aois.from_imscroll_aoiinfo2(TEST_DATA_DIR / "mapping/L2_aoi.dat")
        aois.channel = from_channel
        with pytest.raises(ValueError) as exception_info:
            new_aois = mapper.map(aois, to_channel=to_channel)
            assert (
                exception_info.value
                == "From-channel is not one of the available channels"
            )

    from_channel = "red"
    channel = "blue"
    aois = imp.Aois.from_imscroll_aoiinfo2(TEST_DATA_DIR / "mapping/L2_aoi.dat")
    aois.channel = from_channel

    new_aois = mapper.map(aois, to_channel=channel)
    assert isinstance(new_aois, imp.Aois)
    assert new_aois.frame == aois.frame
    assert new_aois.frame_avg == aois.frame_avg
    assert new_aois.width == aois.width
    assert new_aois.channel == channel
    correct_aois = imp.Aois.from_imscroll_aoiinfo2(
        TEST_DATA_DIR / "mapping/L2_inv_map.dat"
    )
    np.testing.assert_allclose(new_aois._coords, correct_aois._coords, rtol=1e-6)


def test_find_paired_spots_with_unknown_translation():
    rng = np.random.default_rng(seed=0)
    aois_a = imp.Aois(rng.uniform(high=500, size=(100, 2)), 0)
    aois_a = aois_a.remove_close_aois(5)
    spots_a = aois_a.coords
    spots_b = np.zeros((100, 2))
    spots_b[:70, :] = spots_a[:70, :] + np.array([[200, 200]])
    # Need to make sure all spots are separated by at least 5 pixels
    for i in range(70, 100):
        while True:
            new_aoi = imp.Aois(rng.uniform(low=200, high=700, size=(1, 2)), 0)
            if not new_aoi.is_in_range_of(aois_a, 5):
                break
        spots_b[i, :] = new_aoi.coords
    pair_idx = mapping.find_paired_spots_with_unknown_translation(spots_a, spots_b)
    np.testing.assert_equal(pair_idx, np.stack((np.arange(70), np.arange(70)), axis=1))


def test_find_paired_spots_with_unknown_translation_reversed():
    rng = np.random.default_rng(seed=0)
    aois_a = imp.Aois(rng.uniform(high=500, size=(100, 2)), 0)
    aois_a = aois_a.remove_close_aois(5)
    spots_a = aois_a.coords
    spots_b = np.zeros((100, 2))
    spots_b[:70, :] = spots_a[:70, :] + np.array([[200, 200]])
    # Need to make sure all spots are separated by at least 5 pixels
    for i in range(70, 100):
        while True:
            new_aoi = imp.Aois(rng.uniform(low=200, high=700, size=(1, 2)), 0)
            if not new_aoi.is_in_range_of(aois_a, 5):
                break
        spots_b[i, :] = new_aoi.coords
    # Reversing the order of b
    spots_b = np.flip(spots_b, axis=0)
    pair_idx = mapping.find_paired_spots_with_unknown_translation(spots_a, spots_b)
    np.testing.assert_equal(
        pair_idx, np.stack((np.arange(70), 99 - np.arange(70)), axis=1)
    )


def test_find_paired_spots_with_unknown_translation_with_noise():
    rng = np.random.default_rng(seed=0)
    aois_a = imp.Aois(rng.uniform(high=500, size=(100, 2)), 0)
    aois_a = aois_a.remove_close_aois(5)
    spots_a = aois_a.coords
    spots_b = np.zeros((100, 2))
    spots_b[:70, :] = spots_a[:70, :] + np.array([[200, 200]])
    # Add noise to transformed coordinates
    spots_b += rng.normal(scale=0.4, size=spots_b.shape)
    # Need to make sure all spots are separated by at least 5 pixels
    for i in range(70, 100):
        while True:
            new_aoi = imp.Aois(rng.uniform(low=200, high=700, size=(1, 2)), 0)
            if not new_aoi.is_in_range_of(aois_a, 5):
                break
        spots_b[i, :] = new_aoi.coords
    pair_idx = mapping.find_paired_spots_with_unknown_translation(spots_a, spots_b)
    np.testing.assert_equal(pair_idx, np.stack((np.arange(70), np.arange(70)), axis=1))


def test_find_paired_spots_with_unknown_translation_with_noise_shuffled():
    rng = np.random.default_rng(seed=0)
    aois_a = imp.Aois(rng.uniform(high=500, size=(100, 2)), 0)
    aois_a = aois_a.remove_close_aois(5)
    spots_a = aois_a.coords
    spots_b = np.zeros((100, 2))
    spots_b[:70, :] = spots_a[:70, :] + np.array([[200, 200]])
    # Add noise to transformed coordinates
    spots_b += rng.normal(scale=0.4, size=spots_b.shape)
    # Need to make sure all spots are separated by at least 5 pixels
    for i in range(70, 100):
        while True:
            new_aoi = imp.Aois(rng.uniform(low=200, high=700, size=(1, 2)), 0)
            if not new_aoi.is_in_range_of(aois_a, 5):
                break
        spots_b[i, :] = new_aoi.coords
    # Shuffles spots_b
    shuffle = np.arange(100)
    rng.shuffle(shuffle)
    spots_b = spots_b[shuffle, :]
    pair_idx = mapping.find_paired_spots_with_unknown_translation(spots_a, spots_b)
    np.testing.assert_equal(
        pair_idx, np.stack((np.arange(70), np.argsort(shuffle)[:70]), axis=1)
    )


def test_mapper_from_npz():
    map_matrix = np.array([[1, 0, 100], [0, 1, 10]])
    rng = np.random.default_rng(seed=0)
    coords1 = rng.uniform(high=500, size=(100, 2))
    coords2 = mapping.affine_transform(map_matrix[:, :2], map_matrix[:, 2], coords1.T).T
    f = io.BytesIO()
    items = {"green-red": np.concatenate((coords1, coords2), axis=1)}
    np.savez(f, **items)
    f.flush()
    f.seek(0)
    mapper = mapping.Mapper.from_npz(f)
    np.testing.assert_allclose(
        mapper.map_matrix[("green", "red")], map_matrix, rtol=0, atol=1e-11
    )


def test_mapper_bare_from_npz():
    map_matrix = np.array([[1, 0, 100], [0, 1, 10]])
    rng = np.random.default_rng(seed=0)
    coords1 = rng.uniform(high=500, size=(100, 2))
    coords2 = mapping.affine_transform(map_matrix[:, :2], map_matrix[:, 2], coords1.T).T
    f = io.BytesIO()
    items = {"green-red": np.concatenate((coords1, coords2), axis=1)}
    np.savez(f, **items)
    f.flush()
    f.seek(0)
    mapper = mapping.MapperBare.from_npz(f)
    np.testing.assert_allclose(mapper.map_matrix, map_matrix, rtol=0, atol=1e-11)


def test_idl_polynomial_estimation():
    # over-determined
    tform = mapping.IDLPolynomialTransform.from_estimate(SRC, DST, degree=9)
    np.testing.assert_allclose(tform(SRC), DST, rtol=1e-6, atol=1e-3)


def test_idl_polynomial_weighted_estimation():
    # Over-determined solution with same points, and unity weights
    tform = mapping.IDLPolynomialTransform.from_estimate(SRC, DST, degree=10)
    tform_w = mapping.IDLPolynomialTransform.from_estimate(
        SRC, DST, degree=10, weights=np.ones(SRC.shape[0])
    )
    np.testing.assert_allclose(tform.params, tform_w.params)

    # Repeating a point, but setting its weight small, should give nearly
    # the same result.
    point_weights = np.ones(SRC.shape[0] + 1)
    point_weights[0] = 1.0e-15
    tform1 = mapping.IDLPolynomialTransform.from_estimate(SRC, DST, degree=10)
    tform2 = mapping.IDLPolynomialTransform.from_estimate(
        SRC[np.arange(-1, SRC.shape[0]), :],
        DST[np.arange(-1, SRC.shape[0]), :],
        degree=10,
        weights=point_weights,
    )
    np.testing.assert_allclose(tform1.params, tform2.params, atol=3e-5)


def test_idl_polynomial_init():
    tform = mapping.IDLPolynomialTransform.from_estimate(SRC, DST, degree=10)
    # init with transformation parameters
    tform2 = mapping.IDLPolynomialTransform(tform.params)
    np.testing.assert_allclose(tform2.params, tform.params)


def test_idl_polynomial_default_degree():
    tform = mapping.IDLPolynomialTransform.from_estimate(SRC, DST)
    tform2 = mapping.IDLPolynomialTransform.from_estimate(SRC, DST, degree=2)
    np.testing.assert_allclose(tform2.params, tform.params)


def test_idl_polynomial_inverse():
    with pytest.raises(NotImplementedError):
        mapping.IDLPolynomialTransform().inverse(0)


def test_idl_polynomial_idl_data():
    rng = np.random.default_rng(0)
    n = 1000
    test_data = rng.uniform(low=0, high=512, size=(n, 2))
    true_coords = np.loadtxt(TEST_DATA_DIR / "idl/IDL_mapped_coordinates.txt")
    transform = mapping.IDLPolynomialTransform.from_idl_script_map_file(
        TEST_DATA_DIR / "idl/rough.map"
    )
    mapped = transform(test_data)
    np.testing.assert_allclose(mapped, true_coords, rtol=2e-5)


def test_idl_mapper_from_npz():
    mapper = mapping.IDLMapper.from_npz(TEST_DATA_DIR / "mapping/20220328_map.npz")
    with np.load(TEST_DATA_DIR / "mapping/20220328_map.npz") as f:
        coords = f["green-red"]
    test_data = np.loadtxt(TEST_DATA_DIR / "mapping/20220328_map_idl.txt")
    transform = mapper.get_transform(
        mapping.MapDirection(from_channel="green", to_channel="red")
    )
    # Transform matrices should equal to POLYWARP output
    np.testing.assert_allclose(transform.params[0], test_data[:4, :], rtol=3e-2)
    np.testing.assert_allclose(transform.params[1], test_data[4:, :], rtol=3e-2)
    transform_from_idl = mapping.IDLPolynomialTransform(
        (test_data[:4, :], test_data[4:, :])
    )
    # Transform result should be close to each other
    np.testing.assert_allclose(
        transform(coords[:, :2]), transform_from_idl(coords[:, :2]), rtol=5e-5
    )
    green_aois = imp.Aois(coords[:, :2], 0, channel="green")
    green_mapped = mapper.map(green_aois, to_channel="red")
    np.testing.assert_allclose(
        green_mapped.coords, transform_from_idl(coords[:, :2]), rtol=5e-5
    )

    # Reversed direction
    test_data = np.loadtxt(TEST_DATA_DIR / "mapping/20220328_map_idl_inv.txt")
    transform = mapper.get_transform(
        mapping.MapDirection(from_channel="red", to_channel="green")
    )
    # Transform matrices should equal to POLYWARP output
    np.testing.assert_allclose(transform.params[0], test_data[:4, :], rtol=1e-2)
    np.testing.assert_allclose(transform.params[1], test_data[4:, :], rtol=1e-2)
    transform_from_idl = mapping.IDLPolynomialTransform(
        (test_data[:4, :], test_data[4:, :])
    )
    # Transform result should be close to each other
    np.testing.assert_allclose(
        transform(coords[:, 2:4]), transform_from_idl(coords[:, 2:4]), rtol=3e-5
    )
    red_aois = imp.Aois(coords[:, 2:4], 0, channel="red")
    red_mapped = mapper.map(red_aois, to_channel="green")
    np.testing.assert_allclose(
        red_mapped.coords, transform_from_idl(coords[:, 2:4]), rtol=3e-5
    )
