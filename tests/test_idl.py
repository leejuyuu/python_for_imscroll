import math
from pathlib import Path

import numpy as np
import pytest
import skimage.exposure

import blink.idl
import blink.image_processing as imp
import blink.mapping

TEST_DATA_DIR = Path(__file__).parent / "test_data/idl"
N_SETS = 3

LEFT = (slice(0, 512), slice(0, 256))
RIGHT = (slice(0, 512), slice(256, 512))
INNER_R = 3.6
OUTER_R = 4.2
MAGIC = 0.45
EDGE_PERCENTAGE = 0.03


def test_smooth():
    for i in range(1, N_SETS + 1):
        directory = TEST_DATA_DIR / f"set_{i}"
        true_smoothed = np.loadtxt(directory / "smoothed_image.txt")
        image_sequence = imp.ImageSequence(directory)
        avg_im = image_sequence.get_averaged_image(0, 15)
        with pytest.raises(ValueError):
            # _smooth_truncate should only accept odd number
            # because IDL SMOOTH function silently add one to even numbers
            blink.idl._smooth_truncate(avg_im, 2)
        smoothed_im = blink.idl._smooth_truncate(avg_im, 3)
        # Add more tolerance because of the overflow bug in IDL script reduces precision
        np.testing.assert_allclose(smoothed_im, true_smoothed - 32768, rtol=2e-5)

        aves_before_smooth = np.loadtxt(directory / "aves_before_smooth.txt")
        true_aves = np.loadtxt(directory / "aves.txt")
        aves = blink.idl._smooth_truncate(aves_before_smooth, 21)
        np.testing.assert_allclose(aves, true_aves, rtol=2e-7)


def test_pick_spot_from_combined_image():
    circle_factor = 0.45
    width = 512
    height = 512
    combined_im_threshold = 25
    for i in range(1, N_SETS + 1):
        directory = TEST_DATA_DIR / f"set_{i}"
        combined_im = np.loadtxt(directory / "IDL_combined_image.txt", dtype=np.int32)
        real_peak = blink.idl._pick_spot_from_combined_image(
            combined_im,
            combined_im_threshold,
            INNER_R,
            OUTER_R,
            circle_factor,
        )
        # These are the conditions that IDL script made
        cond = np.logical_and.reduce(
            (
                real_peak[:, 0] >= math.ceil(EDGE_PERCENTAGE * width),
                real_peak[:, 0] <= math.floor(MAGIC * width),
                real_peak[:, 1] >= round(EDGE_PERCENTAGE * height),
                real_peak[:, 1] <= round(height - EDGE_PERCENTAGE * height),
            )
        )
        real_peak = real_peak[cond]
        true_peaks = np.loadtxt(directory / "peak_center.txt")
        np.testing.assert_equal(real_peak, true_peaks[:, :2].astype(np.int32))


def test_make_block_background():
    for i in range(1, N_SETS + 1):
        directory = TEST_DATA_DIR / f"set_{i}"
        true_background = np.loadtxt(directory / "aves.txt")
        smoothed_im = np.loadtxt(directory / "smoothed_image.txt")
        block_background = blink.idl._make_block_background(smoothed_im, 16)
        np.testing.assert_allclose(block_background, true_background, rtol=1e-4)


def test_calculate_aoi_background():
    for i in range(1, N_SETS + 1):
        directory = TEST_DATA_DIR / f"set_{i}"
        test_data = np.loadtxt(directory / "peak_center.txt")
        image_sequence = imp.ImageSequence(directory)
        avg_im = image_sequence.get_averaged_image(0, 15)
        real_background = blink.idl.calculate_aoi_background(
            avg_im, test_data[:, :2], 16
        )
        # bkgs is subtracted by 32768 To account for the overflow bug in IDL script
        np.testing.assert_allclose(real_background, test_data[:, 2] - 32768, rtol=3e-3)


def _round_middle_down(x):
    """Round .5 towards 0"""
    if x - math.floor(x) > 0.5:
        return math.ceil(x)
    return math.floor(x)


def test_analyze_peaks():
    for i in range(1, N_SETS + 1):
        directory = Path(TEST_DATA_DIR / f"set_{i}")
        peaks = np.loadtxt(directory / "1.pks")
        true_traces = blink.idl.load_traces(directory / "hel1.traces")
        image_sequence = imp.ImageSequence(directory)
        # bkgs is subtracted by 32768 To account for the overflow bug in IDL script
        bkgs = peaks[:, 3] - 32768
        peaks = peaks[:, 1:3]
        traces = np.zeros((image_sequence.length, peaks.shape[0]))
        aois = imp.Aois(peaks, 0, width=7)
        sigma = 1.29099
        for frame, image in enumerate(image_sequence):
            backgrounds = np.zeros(len(aois))
            for (i, aoi), bkg in zip(enumerate(aois.iter_objects()), bkgs):
                backgrounds[i] = bkg * np.sum(
                    aoi._make_gaussian_kernel(sigma, round_f=_round_middle_down)
                )
            # Multiply by 2 because the kernel in IDL script is multiplied by 2 (has max 2)
            traces[frame, :] = 2 * (
                aois.get_gaussian_weighted_intensity(
                    image, sigma, round_f=_round_middle_down
                )
                - backgrounds
            )
        # Discard the first AOI because the IDL script failed to calculate the correct peak offset for it
        np.testing.assert_allclose(traces[:, 1:], true_traces[:, 1:], atol=1)


def test_find_peaks():
    # The test_data is manually written by checking where the spot picking fails
    # I verified that these are also good spots
    test_data = {
        1: np.ones(127, dtype=bool),
        2: np.ones(87, dtype=bool),
        3: np.ones(80, dtype=bool),
    }
    # This spot is not found by IDL script because the circle check
    test_data[1][6] = False
    # These spots are not found by IDL script because the circle check
    test_data[2][[5, 43]] = False
    # This spot is not found by IDL script because it is too bright and image overflowed
    test_data[2][50] = False
    # These spots are not found by IDL script because the circle check
    test_data[3][[8, 18]] = False

    for i in range(1, N_SETS + 1):
        directory = TEST_DATA_DIR / f"set_{i}"
        true_peak = np.loadtxt(
            directory / "peak_center.txt", usecols=(0, 1), dtype=np.int64
        )
        true_aois = imp.Aois(true_peak, 0)
        image_sequence = imp.ImageSequence(directory)
        transform = blink.mapping.IDLPolynomialTransform.from_idl_script_map_file(
            TEST_DATA_DIR / "rough.map"
        )
        averaged_image = image_sequence.get_averaged_image(0, 15)
        height, width = averaged_image.shape
        scaled_im = skimage.exposure.rescale_intensity(
            averaged_image, out_range=np.uint8
        )
        split = blink.idl.ImageSplit(
            channel_a_idx=RIGHT, channel_b_idx=LEFT, transform_b_to_a=transform
        )
        real_aois = blink.idl.find_peaks(scaled_im, split)
        # These are the conditions that IDL script made
        # Slightly relax the comparison because find_peaks() do gaussian_refine
        cond = np.logical_and.reduce(
            (
                real_aois.coords[:, 0] >= math.ceil(EDGE_PERCENTAGE * width) - 1,
                real_aois.coords[:, 0] <= math.floor(MAGIC * width) + 1,
                real_aois.coords[:, 1] >= round(EDGE_PERCENTAGE * height) - 1,
                real_aois.coords[:, 1] <= round(height - EDGE_PERCENTAGE * height) + 1,
            )
        )
        real_aois.coords = real_aois.coords[cond]
        # The goal is to pick no more spurious spot than IDL script,
        # so the spots picked by blink should be close to some IDL found ones
        np.testing.assert_equal(real_aois.is_in_range_of(true_aois, 4), test_data[i])


def test_generate_circular_mask():
    circle = [
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ]
    idx = blink.idl._generate_circular_mask(INNER_R, OUTER_R)
    np.testing.assert_equal(
        np.array(circle)[idx[0] + 5, idx[1] + 5],
        np.ones(np.count_nonzero(circle), dtype=bool),
    )
