import pandas as pd


def read_excel(filepath, sheet_name=0):
    # TODO: is this read_only flag still necessary?
    # doesn't break any tests even if omitted
    return pd.read_excel(
        filepath, sheet_name=sheet_name, engine_kwargs=dict(read_only=False)
    )
