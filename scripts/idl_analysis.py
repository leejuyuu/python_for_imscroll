from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

import blink.image_processing as imp
import blink.mapping
import blink.time_series as ts
from blink import idl


def main():
    FRAME_AVERAGE = 10
    LEFT = (slice(0, 512), slice(0, 256))
    RIGHT = (slice(0, 512), slice(256, 512))

    left_channel = ("green", "green")
    right_channel = ("green", "red")
    sigma = 1.3
    width = 7

    image_path = Path(r"D:\IDL_test_data\L1_DNA2_HLTF_+BCDX2_view 1\rxn")
    sequence = imp.ImageSequence(image_path)
    if sequence.length < FRAME_AVERAGE:
        raise ValueError(
            f"Image sequence length ({sequence.legth}) not long enough to averge"
            f" {FRAME_AVERAGE} frames."
        )
    transform = blink.mapping.IDLPolynomialTransform.from_idl_script_map_file(
        Path(r"D:\blink\tests\test_data\idl\rough.map")
    )
    averaged_image = sequence.get_averaged_image(0, FRAME_AVERAGE)
    split = idl.ImageSplit(
        channel_a_idx=RIGHT, channel_b_idx=LEFT, transform_b_to_a=transform
    )
    aois = idl.find_peaks(
        averaged_image, split, frame=0, frame_avg=FRAME_AVERAGE, width=width
    )
    mapped_aois = imp.Aois(
        transform(aois.coords) + np.array([[256, 0]]),
        frame=0,
        frame_avg=FRAME_AVERAGE,
        width=width,
    )

    # Need to load the averaged_image again because find_peaks mutates the image
    averaged_image = sequence.get_averaged_image(0, FRAME_AVERAGE)
    bkgs = idl.calculate_aoi_background(averaged_image, aois, 16)
    background = np.zeros(len(aois))
    for (i, aoi), bkg in zip(enumerate(aois.iter_objects()), bkgs):
        background[i] = bkg * np.sum(aoi._make_gaussian_kernel(sigma))

    bkgs = idl.calculate_aoi_background(averaged_image, mapped_aois, 16)
    mapped_background = np.zeros(len(mapped_aois))
    for (i, aoi), bkg in zip(enumerate(mapped_aois.iter_objects()), bkgs):
        mapped_background[i] = bkg * np.sum(aoi._make_gaussian_kernel(sigma))
    time_arr = sequence.time - sequence.time[0]
    traces = ts.TimeTraces(
        n_traces=len(aois), channels={left_channel: time_arr, right_channel: time_arr}
    )
    for i, image in enumerate(tqdm(sequence, total=sequence.length)):
        time = time_arr[i]
        intensity = aois.get_gaussian_weighted_intensity(image, sigma=sigma)
        traces.set_value(
            "raw_intensity", channel=left_channel, time=time, array=intensity
        )
        traces.set_value(
            "background", channel=left_channel, time=time, array=background
        )
        traces.set_value(
            "intensity", channel=left_channel, time=time, array=intensity - background
        )

        intensity = mapped_aois.get_gaussian_weighted_intensity(image, sigma=sigma)
        traces.set_value(
            "raw_intensity", channel=right_channel, time=time, array=intensity
        )
        traces.set_value(
            "background", channel=right_channel, time=time, array=mapped_background
        )
        traces.set_value(
            "intensity",
            channel=right_channel,
            time=time,
            array=intensity - mapped_background,
        )

    traces.to_npz(Path("~/traces.npz").expanduser())
    fig, axes = plt.subplots(nrows=3)
    save_dir = Path("~/traces").expanduser()
    if not save_dir.exists():
        save_dir.mkdir()
    time = traces.get_time(("green", "green"))
    for molecule in range(traces.n_traces):
        donor = traces.get_intensity(left_channel, molecule)
        acceptor = traces.get_intensity(right_channel, molecule)
        axes[0].plot(time, donor + acceptor, "k")
        axes[0].set_ylim(bottom=-1000)
        axes[1].plot(time, donor, "g")
        axes[1].plot(time, acceptor, "r")
        axes[1].set_ylim(bottom=-1000)
        axes[2].plot(time, acceptor / (donor + acceptor), "b")
        axes[2].set_ylim((-0.1, 1.1))
        fig.savefig(save_dir / f"{molecule}.png", dpi=300)
        for ax in axes:
            ax.cla()


if __name__ == "__main__":
    main()
